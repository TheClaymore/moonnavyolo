﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoonNavYolo
{
    public static class Detector
    {
        public static void StartDetector(string input, string output, float thresh)
        {
            //Delete file
            if (File.Exists(output))
            {
                try
                {
                    File.Delete(output);
                }
                catch (Exception e) { }
            }

            var mydir = Directory.GetCurrentDirectory().Replace('\\', '/');
            var torun = File.ReadAllLines("darknetcmd.txt")[0];

            torun = torun.Replace("%out%", mydir + "/"+output)
                         .Replace("%image%", mydir + "/"+input)
                         .Replace("%thresh%", thresh.ToString("0.000").Replace(',','.'));

            Console.WriteLine("Executing command:\n" + torun);
            System.Diagnostics.Process.Start("CMD.exe", " /C " + torun);
        }

        public static Results DetectCraters(string input, float thresh)
        {
            var output = "result.json";
            Detector.StartDetector(input, output, thresh);
            while (!Detector.CanReadFile(output)) { Thread.Sleep(500); }
            return Detector.GetResults(output);
        }

        public static async Task<Results> DetectCratersAsync(string input, float thresh)
        {
            var output = "result.json";
            Detector.StartDetector(input, output, thresh);
            while (!Detector.CanReadFile(output)) { await Task.Delay(500); }
            return Detector.GetResults(output);
        }

        public static Results GetResults(string file)
        {
            return new Results(file);
        }

        public static bool CanReadFile(string output)
        {
            if(File.Exists(output))
            {
                if(!IsFileLocked(new FileInfo(output)))
                {
                    return true;
                }
            }
            return false;
        }

        private static bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

    }
}
