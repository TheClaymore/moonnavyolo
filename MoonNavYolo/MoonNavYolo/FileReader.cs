﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MoonNavYolo
{
    public static class FileReader
    {
        public static Database Data;

        public static Database ParseCSV(string dbfile, ChunkType type)
        {
            Data = new Database(type);
            Data.ParseFile(dbfile);
            return Data;
        }

        internal static Vector2 ParseCoordinates(string fname)
        {
            var lines = File.ReadAllLines(fname);
            return new Vector2(0,0);
        }
    }

    public enum ChunkType
    {
        Two,
        deg30,
        deg3
    }
}
