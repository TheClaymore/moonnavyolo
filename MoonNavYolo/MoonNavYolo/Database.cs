﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Numerics;
using System.Text;

namespace MoonNavYolo
{
    public class Database
    {
        private List<Chunk> chunks;

        private ChunkType gen_type;

        private const int c30 = 360 / 30;
        private const int c3 = 360 / 3;

        internal void ParseFile(string dbfile)
        {
            var crtrs = File.ReadAllLines(dbfile);
            var first = true;
            foreach(var line in crtrs)
            {
                if(first)
                {
                    first = false;
                    continue;
                }    
                Crater newcrat = CraterParse(line);
                AddCrater(newcrat);
            }

            return;
        }

        internal void LogStatistics()
        {
            var rnd = new Random();
            Console.WriteLine("Database gen_type="+gen_type+"\nContains "+chunks.Count+" chunks:\n-------------------");
            foreach (var chnk in chunks)
            {
                Console.WriteLine("C: "+chnk.topleft+"; "+chnk.botright+"; count="+ chnk.crtrs.Count);
            }
        }

        private static Crater CraterParse(string line)
        {
            var vals = line.Replace(";", "").Split(' ');
            var newcrat = new Crater(
                    vals[0],
                    double.Parse(vals[1]),
                    double.Parse(vals[2]),
                    double.Parse(vals[3])
                );
            return newcrat;
        }

        //lat = -90 + degstep*i
        //lon = -180 + degstep*i
        private void AddCrater(Crater newcrat)
        {
            int index = 0;
            switch (gen_type)
            {
                case ChunkType.Two:
                    if(newcrat.Latitude > 0)
                    {
                        chunks[0].crtrs.Add(newcrat);
                    }
                    chunks[1].crtrs.Add(newcrat);
                    break;
                case ChunkType.deg30:
                    index += (int)(newcrat.Latitude+90.0) / 30 * c30;
                    index += (int)(newcrat.Longitude) / 30;
                    chunks[index].crtrs.Add(newcrat);
                    break;
                case ChunkType.deg3:
                    index += (int)(newcrat.Latitude + 90.0) / 3 * c3;
                    index += (int)(newcrat.Longitude) / 3;
                    chunks[index].crtrs.Add(newcrat);
                    break;
            }
        }

        public Database(ChunkType ctype)
        {
            gen_type = ctype;
            chunks = new List<Chunk>();
            switch (ctype)
            {
                case (ChunkType.Two):
                    GenerateTwo();
                    break;
                case (ChunkType.deg30):
                    Generate30();
                    break;
                case (ChunkType.deg3):
                    Generate3();
                    break;
            }
        }

        private void GenerateTwo()
        {
            chunks.Add(new Chunk(
                new Vector2(90f, 0f),
                new Vector2(0f, 360f)
            ));
            chunks.Add(new Chunk(
                new Vector2(0f, 0f),
                new Vector2(-90f, 360f)
            ));
        }

        private void Generate30()
        {
            Generate(30);
        }

        private void Generate3()
        {
            Generate(3);
        }

        private void Generate(int degstep)
        {
            for(int lat = -90; lat < 90; lat += degstep)
            {
                for (int lon = 0; lon < 360; lon += degstep)
                {
                    chunks.Add(new Chunk(
                        new Vector2(lat, lon),
                        new Vector2(lat + degstep, lon + degstep)
                    ));
                }
            }
            Console.WriteLine(chunks.Count);
        }
    }

    public class Chunk
    {
        public List<Crater> crtrs;
        public Vector2 topleft;
        public Vector2 botright;
        public Chunk(Vector2 tl, Vector2 br)
        {
            crtrs = new List<Crater>();
            topleft = tl;
            botright = br;

        }
    }

    public struct Crater
    {
        public string Id;
        public double Latitude;
        public double Longitude;
        public double Diameter;

        public Crater(string id, double latitude, double longiture, double diameter)
        {
            Id = id;
            Latitude = latitude;
            Longitude = longiture;
            Diameter = diameter;
        }
    }
}
