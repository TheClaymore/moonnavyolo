﻿using System;

namespace MoonNavYolo
{
    class Program
    {
        static void Main(string[] args)
        {
            YOLOv4MLNet.Searcher.Intitialize();
            Console.WriteLine("YOLOv4.ONNX model loaded successfully.");
            YOLOv4MLNet.Searcher.PredictSave("test.png");
        }
    }
}
