﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace MoonNavYolo
{
    public static class Debug
    {

        public static void DrawDetectionResults(string originalimg, Results res, bool circles, string savedir)
        {
            DrawDetectionResults(originalimg, res.list, circles, savedir);
        }

        public static void DrawDetectionResults(string originalimg, List<YoloCrater> crtrs, bool circles, string savedir)
        {
            var img = (Bitmap)Bitmap.FromFile(originalimg);
            
            using (Graphics gr = Graphics.FromImage(img))
            {
                foreach (var crt in crtrs)
                {
                    var size = Math.Sqrt((crt.width * img.Width + crt.height * img.Height));
                    if (size < 6)
                        size = 6;
                    if (size > 18)
                        size = 18;

                    Pen pen = new Pen(ColorFromConf(crt.confidence), 1f);
                    Font fnt = new Font("Arial", (int)size, FontStyle.Regular);
                    gr.DrawString(crt.confidence.ToString("0.00"), fnt, pen.Brush, new PointF((float)(crt.center_x) * img.Width, (float)(crt.center_y) * img.Height));
                    if(circles)
                    {
                        gr.DrawEllipse(pen, (float)(crt.center_x - crt.width / 2) * img.Width, (float)(crt.center_y - crt.height / 2) * img.Height, (float)crt.width * img.Width, (float)crt.height * img.Height);
                    }
                    else
                    {
                        gr.DrawRectangle(pen, (float)(crt.center_x - crt.width / 2) * img.Width, (float)(crt.center_y - crt.height / 2) * img.Height, (float)crt.width * img.Width, (float)crt.height * img.Height);
                    }
                }
            }

            img.Save(savedir);
        }
        
        

        private static Color ColorFromConf(double conf)
        {
            return Color.FromArgb(
                (int)(conf * 255.0),
                0,
                255 - (int)(conf*255.0)
                );
        }
    }
}
