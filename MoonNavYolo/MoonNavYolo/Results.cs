﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace MoonNavYolo
{
    public class Results
    {
        public List<YoloCrater> list;
        public Results(string path)
        {
            var lines = File.ReadAllLines(path);
            bool startread = false;
            list = new List<YoloCrater>();
            for(int i = 0; i<lines.Length; i++)
            {
                if(startread)
                {
                    if (lines[i].Contains("]"))
                        break;
                    list.Add(CraterFromString(lines[i]));
                }
                if(lines[i].Contains("\"objects\": ["))
                {
                    startread = true;
                }
            }
        }

        //Example string:
        //   {"class_id":0, "name":"YoloCrater", "relative_coordinates":{"center_x":0.323512, "center_y":0.181721, "width":0.131385, "height":0.118109}, "confidence":0.997042},  
        private YoloCrater CraterFromString(string v)
        {
            v = '[' + v + ']';
            JsonTextReader reader = new JsonTextReader(new StringReader(v));

            /*while (reader.Read())
            {
                if (reader.Value != null)
                {
                    Console.WriteLine("Token: {0}, Value: {1}", reader.TokenType, reader.Value);
                }
                else
                {
                    Console.WriteLine("Token: {0}", reader.TokenType);
                }
            }*/

            reader.Read(); //StartArray
            reader.Read(); //StartObject
            reader.Read(); //class_id
            reader.Read(); //Integer
            reader.Read(); //name
            reader.Read(); //crater
            reader.Read(); //relative_coordinates
            reader.Read(); //StartObject

            reader.Read(); //center_x
            reader.Read(); //Actual value
            var cx = (double)reader.Value;

            reader.Read(); //center_y
            reader.Read(); //Actual value
            var cy = (double)reader.Value;

            reader.Read(); //width
            reader.Read(); //Actual value
            var w = (double)reader.Value;

            reader.Read(); //height
            reader.Read(); //Actual value
            var h = (double)reader.Value;

            reader.Read(); //EndObject

            reader.Read(); //confidence
            reader.Read(); //Actual value
            var conf = (double)reader.Value;

            return new YoloCrater(cx, cy, w, h, conf);
        }

        private static string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            int place = Source.LastIndexOf(Find);

            if (place == -1)
                return Source;

            string result = Source.Remove(place, Find.Length).Insert(place, Replace);
            return result;
        }

        public List<YoloCrater> ReturnBiggest(int n)
        {
            var listlcl = new List<YoloCrater>();
            for(int i = 0; i<n && i<list.Count; i++)
            {
                listlcl.Add(list[i]);
            }
            return listlcl;
        }

    }

    public struct YoloCrater
    {
        //{"center_x":0.323512, "center_y":0.181721, "width":0.131385, "height":0.118109}, "confidence":0.997042}

        public double center_x;
        public double center_y;
        public double width;
        public double height;
        public double confidence;

        public YoloCrater(double center_x, double center_y, double width, double height, double confidence)
        {
            this.center_x = center_x;
            this.center_y = center_y;
            this.width = width;
            this.height = height;
            this.confidence = confidence;
        }
    }
}